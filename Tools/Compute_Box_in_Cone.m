function v_max = Compute_Box_in_Cone(Tmax,epmax)
% This function compute numerically the largest box incribed in the given
% set Vc
%  Tmax : Upperbound of the thrust provided (m/s^2)
%  epmax: Limit for roll and pitch angles (rad)
N = 3; % the number of zonotope's generator
gamma = -2*(dec2bin(0:2^N-1)' - '0')+1;
g=9.81;
sdpvar vB1 vB2 vB3
constraints=[vB1>=0, vB2>=0, vB3>=0];
Vertex_set=[];
for i=1:size(gamma,2)
    tmpp=gamma(:,i);
    Vertex_set=[Vertex_set; tmpp'*diag([vB1 vB2 vB3])'];
end
for i=1:size(Vertex_set,1)
    v_vertex=Vertex_set(i,:)';
    constraints=[constraints, ...
        (v_vertex(1)^2 + v_vertex(2)^2 + (v_vertex(3)+g)^2)<=Tmax^2,...
        (v_vertex(1)^2+v_vertex(2)^2) <=(v_vertex(3)+g)^2*tan(epmax)^2,...
        v_vertex(3)>=-g        ];
end
options=sdpsettings('solver','fmincon');% for volume criterion due to nonlinearity
% Maximize the volume
objective=vB1* vB2* vB3;
diagnostics = optimize(constraints,-objective,options);
v_max= value([vB1 vB2 vB3]);
end

