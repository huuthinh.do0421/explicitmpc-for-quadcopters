function u=computeUfromV(v,psi)
g=9.81;
if size(v,2)>1 % array-wise computation
    v1 = v(1,:);v2 = v(2,:);v3 = v(3,:);
    T = sqrt(v1.^2+v2.^2+(v3+g).^2);
    phi = asin((v1.*sin(psi) - v2.*cos(psi))./T);
    theta = atan((v1.*cos(psi) + v2.*sin(psi))./(v3+g));
else %single value
    v1 = v(1);v2 = v(2);v3 = v(3);
    T = sqrt(v1^2+v2^2+(v3+g)^2);
    phi = asin(1e-6+(v1*sin(psi) - v2*cos(psi))/T);
    theta = atan(1e-6+(v1*cos(psi) + v2*sin(psi))/(v3+g));
end
u = [T;phi;theta];
end


