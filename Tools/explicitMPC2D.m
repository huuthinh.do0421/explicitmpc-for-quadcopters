classdef explicitMPC2D < handle
    properties (SetAccess = private)
        nz;nx;H;He;ni;ne;fH;fF;fg;tH;tF;tg;
    end
    methods (Access = public)
        function obj = explicitMPC2D(data_regions)
            list_prop=fieldnames(data_regions);
            for i=1:numel(list_prop)
                cmd_prop = ['obj.',list_prop{i},'=', 'data_regions.(list_prop{i});'];
                eval(cmd_prop);
            end
        end
        %% Evaluate the explicit MPC
        function [z,i] = eval_predict(obj, x)
            x=x(:);xh=[x;-1];
            for i=1:(size(obj.ne,1)-1)
                if all(obj.H(obj.ni(i):obj.ni(i+1)-1,:)*xh<=1e-08)
                    z=obj.fF((i-1)*obj.nz+1:i*obj.nz,:)*x+obj.fg((i-1)*obj.nz+1:i*obj.nz);
                    return
                end
            end
            i=0;z=NaN(obj.nz,1);
        end
        function [u,regid]=evaluate(obj,x)
            [ul, regid]=eval_predict(obj, x);
            u=ul(1);
        end
    end
    
    
end