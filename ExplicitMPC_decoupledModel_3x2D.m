clc
clear
close all
addpath(genpath('Tools'))
ax_list={'x','y','z'};

g=9.81;
Tmax=1.45*g; epmax=10*pi/180;
vB = Compute_Box_in_Cone(Tmax, epmax);
%% system description
% continuous
A1 = [0 1; 0 0];
B1 = [0; 1];
% discretization
Ts=0.1;
Ad = (eye(2) - A1*Ts/2)^-1 * (eye(2) + A1*Ts/2);
Bd = (eye(2) - A1*Ts/2)^-1 * B1*Ts;
Q=diag([50,5]);
R=10;
for i_di = 1:3
% workspace
if i_di==3
X_ws =  Polyhedron('ub',[1.5,1.5],...
    'lb',-[1.5,1.5]);
else
    X_ws =  Polyhedron('ub',[1.5,1],...
    'lb',-[1.5,1]);
end
Npred = 30; % prediction horizon
model = LTISystem('A', Ad, 'B', Bd);
controller = MPCController(model, Npred);
controller.model.x.with('setConstraint');
controller.model.x.setConstraint = X_ws;
controller.model.u.with('setConstraint');
controller.model.u.setConstraint = Polyhedron('ub', vB(i_di), 'lb', - vB(i_di));
controller.model.x.penalty = QuadFunction(Q);
controller.model.u.penalty = QuadFunction(R);
Xf = controller.model.LQRSet;
% Compute terminal weight P
P = controller.model.LQRPenalty;
% Add terminal set and terminal penalty
controller.model.x.with('terminalSet');
controller.model.x.terminalSet = Xf;
controller.model.x.with('terminalPenalty');
controller.model.x.terminalPenalty = P;
% Compute the explicit solution
explicit_solution = controller.toExplicit(); 
ExMPC{i_di}= explicit_solution;
end
%% plot results
close all
figure('units','normalized','outerposition',[0 0 1 1])
for i=1:3
subplot(2,3,i+3)
ExMPC{i}.feedback.fplot('colormap', 'turbo','alpha',0.7)
view([77 46])
xlabel(['$' ax_list{i} '$ (m)'], 'interpreter', 'latex','fontsize', 18)
ylabel(['$ \dot ' ax_list{i} '$ (m/s)'], 'interpreter', 'latex','fontsize', 18)
zlabel(['$v_', num2str(i) ,'$'], 'interpreter', 'latex','fontsize', 18)
end

for i=1:3
subplot(2,3,i)
ExMPC{i}.cost.fplot('colormap', 'turbo', 'Edgealpha',0)
view([15 28])
xlabel(['$' ax_list{i} '$ (m)'], 'interpreter', 'latex','fontsize', 18)
ylabel(['$ \dot ' ax_list{i} '$ (m/s)'], 'interpreter', 'latex','fontsize', 18)
zlabel(['$J_', num2str(i) ,'(\cdot)$'], 'interpreter', 'latex','fontsize', 18)
end


