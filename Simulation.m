clc
clear
close all
addpath(genpath('Tools'))
load('ExMPC.mat')
ax_list={'x','y','z'};

%% Controller setup
cltr2Dx=explicitMPC2D(load('Tools/data_controllers/exmpc_x_Np30'));
cltr2Dy=explicitMPC2D(load('Tools/data_controllers/exmpc_y_Np30'));
cltr2Dz=explicitMPC2D(load('Tools/data_controllers/exmpc_z_Np30'));
%% System setup
nx = 6;
nu = 3;
X_init = [1;0.7;0.5;0.6; 0.2;0.2];
Ts = 0.1;
Nsim = 600;
u = zeros(nu, Nsim);
v = zeros(nu, Nsim);

psi = 0; %yaw angle
X = [X_init, zeros(nx, Nsim)];
%% Simulation loop
% Drone_translational_dynamics(psi,xk,uk,Ts)
for k=1:Nsim
    Xk = X(:, k);
    %input in the flat output space
    v1 = cltr2Dx.evaluate(Xk([1 4]));
    v2 = cltr2Dy.evaluate(Xk([2 5]));
    v3 = cltr2Dz.evaluate(Xk([3 6]));
    v(:,k) = [v1; v2; v3];
    % real input
    u(:,k) = computeUfromV([v1; v2; v3], psi);
    X(:, k+1) = Drone_translational_dynamics(psi,Xk,u(:,k),Ts);
end

%%
close all
figure('units','normalized','outerposition',[0 0 1 1])
for i=1:3
    dim =  [1 4]+i-1;
subplot(2,3,i)
ExMPC{i}.feedback.fplot('colormap', 'turbo','alpha',0.7)
hold on
plot3(X(dim(1),1:end-1  ),X(dim(2),1:end-1  ),v(i, :),'color', 'm','linewidth',3)
view([77 46])
xlabel(['$' ax_list{i} '$ (m)'], 'interpreter', 'latex','fontsize', 18)
ylabel(['$ \dot ' ax_list{i} '$ (m/s)'], 'interpreter', 'latex','fontsize', 18)
zlabel(['$v_', num2str(i) ,'$'], 'interpreter', 'latex','fontsize', 18)
end
subplot(2,3,4:6)
cnt=1;
view(3)
% axis square
xlim([-0.5 1.5]);ylim([-0.6 1]);zlim([-.1 0.8])
hold on
plot3(X(1,:),X(2,:),X(3,:),'linewidth',2,'linestyle','--')
while cnt <=Nsim && norm(X(:,cnt))>=1e-5
drone_plot_w(X(1,cnt),X(2,cnt),X(3,cnt),...
    u(2,cnt),u(3,cnt),psi,0.05)
cnt = cnt + 4;
xlabel('$x$ (m)', 'interpreter', 'latex','fontsize', 18)
ylabel('$y$ (m)', 'interpreter', 'latex','fontsize', 18)
zlabel('$z$ (m)', 'interpreter', 'latex','fontsize', 18)
grid on
view([-50 46])
drawnow();
end




