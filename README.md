# ExplicitMPC for Quadcopters

## Control synthesis
The project presents the construction and simulation for the Explicit MPC with the application of quadcopters' **position control** (outer loop control).

The control synthesis is conducted with the following steps:
1. Linearize the model in closed-loop with the linearizing law deduced from the system's flat representation;
2. Construct the new constraint set in the flat output space;
3. Formulate the Model Predictive Control optimization problem for the linearized system in the flat output space;
4. Enumerate critical regions, namely, solve the MPC explicitly as a multi-parametric Quadratic Program [1][2]. 
![alt text](Img/ExMPC.png)


## Required Matlab toolbox
*   [ ] [MPT3](https://www.mpt3.org/)
*   [ ] Yalmip (Usually attached with MPT3)

## Implementation with Matlab
- Run the file `ExplicitMPC_6D_model.m` to construct the Explicit MPC solution for the six dimensional linearized system. With this representation, the computational power required is extremely heavy. Alternatively, we decouple the dynamics into 3 subsystems.
- Run `ExplicitMPC_decoupledModel_3x2D.m` to construct the Explicit MPC for the decoupled representation. Finally, run `Simulation.m` to verify the control scheme via a set-point tracking simulation.

## Experiment video 
The validation with Crazyflie nano-drones can be found [here](https://youtu.be/u7PsNDheIR4?si=KAlskyoaF00ldq4b):
[![ExMPC Crazyflie](Img/thumb.png)](https://youtu.be/u7PsNDheIR4?si=KAlskyoaF00ldq4b)

## License
[![License: CC BY-NC-ND 4.0](https://licensebuttons.net/l/by-nc-nd/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc-nd/4.0/)

## Reference
[1] Bemporad, Alberto, Manfred Morari, Vivek Dua, and Efstratios N. Pistikopoulos. "The explicit linear quadratic regulator for constrained systems." Automatica 38, no. 1 (2002): 3-20. Available [here](http://cse.lab.imtlucca.it/~bemporad/publications/papers/automatica-mpqp.pdf).

[2] Takács, Bálint, Juraj Števek, Richard Valo, and Michal Kvasnica. "Python code generation for explicit MPC in MPT." In 2016 European Control Conference (ECC), pp. 1328-1333. IEEE, 2016.



