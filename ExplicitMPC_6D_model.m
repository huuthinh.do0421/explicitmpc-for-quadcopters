clc
clear
close all
%% System parameters
g=9.81;
Tmax=1.45*g; epmax=10*pi/180;
g=9.81;
T=1.45*g;
%% Construct the constraint set in the flat output space
l_1 = 5;
l_2 = 2;
R=T*sin(epmax);
alpha=linspace(0,2*pi,l_1);
r = linspace(0,R,l_2);
v3 =T*cos(epmax)-g ;
points=[0,0,-g];
for i = 1:l_1
    for j = 1:l_2
        alpha_i = alpha(i);
        r_j = r(j);
        points = [points;
            [r_j*cos(alpha_i),r_j*sin(alpha_i),sqrt(T^2-r_j^2)-g]];
        points = [points; [R*cos(alpha_i),R*sin(alpha_i),v3]];
        
    end
end
Vc_tilde=Polyhedron('V',points);
opt={'interpreter','latex','fontsize',20};
figure
view(3)
hold on
vc=plot(Vc_tilde,'color','g','edgealpha',0.,'edgecolor','k','alpha',0.3);
scatter3(points(:,1),points(:,2),points(:,3),'filled')
camlight('right');
light('Position',[-5 0 9],'Style','local');
view([-38 11.8])
xlim([-4 4])
ylim([-4 4])
zlim([-g v3+0.5])
%% LTI model
% The discretized model of the three concatenated double integrators
Ts= 0.1; % sampling time (seconds)
Ad = [eye(3), Ts*eye(3);
    zeros(3), eye(3)];
Bd= [0.5*(Ts^2)*eye(3); Ts*eye(3)];
%% MPC setup
X_ws =  Polyhedron('ub',[1.5,1.5,1.5,1,1,1.5],...
    'lb',-[1.5,1.5,1.5,1,1,1.5]); %workspace
model = LTISystem('A', Ad, 'B', Bd);

Npred = 2; %prediction horizon
controller = MPCController(model, Npred);
% State constraint
controller.model.x.with('setConstraint');
controller.model.x.setConstraint = X_ws;
% Input constraint for the linearized system in the flat output space
controller.model.u.with('setConstraint');
controller.model.u.setConstraint = Vc_tilde;
% Weighting matrices
controller.model.x.penalty = QuadFunction(blkdiag(50*eye(3),5*eye(3)));
controller.model.u.penalty = QuadFunction(10*eye(3));
Xf = controller.model.LQRSet;
% Compute terminal weight P
P = controller.model.LQRPenalty;
%% Compute the explicit MPC solution
warning('The generation of the MPC solution takes around 24 hours, do you want to continue?')
check_running=input('Type "y" to continue running this code (type any other keys to renounce):  ','s');
if check_running ~='y'
    warning('Rerun the code and type "y" if you want to execute the script')
    error('Program terminated by user')
end
explicit_solution = controller.toExplicit(); 
%% Export to python
opt=explicit_solution.optimizer;
opt.toPython(['ExMPC_controller_Np_', num2str(Npred)],'primal','first-region')


